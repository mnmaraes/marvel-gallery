//
//  HomeUITests.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/27/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import XCTest

class HomeUITests: XCTestCase {

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()

    }

    override func tearDown() {

        super.tearDown()
    }

    // MARK: Expected Behaviors
    /**
     On Cell Select should push the correspondent Details Controller

     also tests that Details successfully returns home
     */
    func testCellSelect() {
        let app = XCUIApplication()
        wait(3)

        let cells = app.cells

        // Pick 5 indexes at Random
        let indexes = pickAtRandom(3, upperBound: cells.count)

        let marvel = app.images["Marvel"]

        for index in indexes {
            // Make Sure We're Home
            XCTAssert(CGRectContainsRect(app.windows.elementBoundByIndex(0).frame, marvel.frame))

            let cell = cells.elementBoundByIndex(index)

            cell.tap()
            wait()

            let name = app.staticTexts.matchingIdentifier("name_label").element

            XCTAssert(name.exists)

            // Return Home
            let button = app.buttons["Pop"]
            button.tap()
            wait()
        }
    }

    /**
     On Load More should load more cells to display
     */
    func testLoadMore() {
        let app = XCUIApplication()

        let initialCount = app.cells.count

        let lastCell = app.cells.elementBoundByIndex(initialCount - 1)

        while !lastCell.isVisible() {
            app.tables.element.swipeUp()
            wait()
        }

        wait(5)
        XCTAssertNotEqual(initialCount, app.cells.count)
    }
}
