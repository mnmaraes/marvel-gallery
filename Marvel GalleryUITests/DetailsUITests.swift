//
//  DetailsUITests.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/27/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import XCTest

class DetailsUITests: XCTestCase {

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()

    }

    override func tearDown() {

        super.tearDown()
    }

    // MARK: Expected Behaviors
    /**
     On Tap Image should display the correspondent Collection Controller
     */
    func testTapImage() {
        let app = XCUIApplication()

        // Get To Details
        wait()
        app.cells.elementBoundByIndex(4).staticTexts.element.tap()
        wait(5)

        // Select a random Collection View
        let collectionView = app.collectionViews
            .elementBoundByIndex(pickOne(app.collectionViews.count))

        app.staticTexts.matchingIdentifier("name_label").element.swipeUp()
        wait(5)

        let cells = collectionView.cells
        let indexes = pickAtRandom(3, upperBound: cells.count - 1)

        for index in indexes {
            let cell = cells.elementBoundByIndex(index)

            cell.tap()
            wait()

            let pageLabel = app.staticTexts.matchingIdentifier("page_indicator").element.label

            // Make Sure Our Labels are Displaying the proper info
            XCTAssert(pageLabel.hasPrefix("\(index)"))

            // Return To Details Page
            app.buttons["Close"].tap()
            wait(2)
        }


    }
}
