//
//  SearchUITests.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/27/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import XCTest

class SearchUITests: XCTestCase {

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()

        wait(3)
        XCUIApplication().buttons["Search"].tap()
        wait(3)
    }

    override func tearDown() {

        super.tearDown()
    }

    // MARK: Expected Behaviors
    /**
     On Cell Select should push the correspondent Details Controller
     */
    func testCellSelect() {
        let app = XCUIApplication()
        wait()
        let cells = app.cells

        // Pick 5 indexes at Random
        let indexes = pickAtRandom(3, upperBound: cells.count)

        let searchBar = app.searchFields.element

        for index in indexes {
            // Make Sure We're at the Search Scree
            XCTAssert(CGRectContainsRect(app.windows.elementBoundByIndex(0).frame, searchBar.frame))

            let cell = cells.elementBoundByIndex(index)

            let selectedLabel = cell.staticTexts.element.label

            cell.tap()
            wait()

            let name = app.staticTexts.matchingIdentifier("name_label").element

            // Make Sure we reached the Details Screen and that it corresponds to the selected cell
            XCTAssert(name.exists)
            XCTAssertEqual(selectedLabel, name.label)

            // Return Home
            let button = app.buttons["Pop"]
            button.tap()
            wait()
        }
    }

    /**
     On Search should update the table view with the filtered results
     */
    func testSearch() {
        let app = XCUIApplication()
        let searchBar = app.searchFields.element

        wait()
        searchBar.tap()
        wait()

        let string = "Ab"

        searchBar.typeText("Ab")
        wait(10)

        let cells = app.cells

        for index in 0..<cells.count {
            let text = cells.elementBoundByIndex(index).staticTexts.element.label

            XCTAssert(text.containsString(string))
        }
    }

    /**
     On Load More should load more cells
     */
    func testLoadMore() {
        let app = XCUIApplication()

        let initialCount = app.cells.count

        let lastCell = app.cells.elementBoundByIndex(initialCount - 1)

        while !lastCell.isVisible() {
            app.tables.element.swipeUp()
            wait()
        }

        wait(5)
        XCTAssertNotEqual(initialCount, app.cells.count)
    }

}
