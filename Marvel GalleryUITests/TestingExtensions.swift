//
//  TestingExtensions.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/27/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import XCTest

func wait(time: NSTimeInterval = 1) {
    NSRunLoop.mainRunLoop().runUntilDate(NSDate(timeIntervalSinceNow: time))
}

func pickOne(upperBound: UInt) -> UInt {
    return UInt(arc4random_uniform(UInt32(upperBound)))
}

func pickAtRandom(count: UInt, upperBound: UInt) -> [UInt] {
    return (0..<count).map({ _ in return pickOne(upperBound) })
}

extension XCUIElement {
    func isVisible() -> Bool {
        let window = XCUIApplication().windows.elementBoundByIndex(0)
        return self.exists && CGRectContainsRect(window.frame, self.frame)
    }
}
