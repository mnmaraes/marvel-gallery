//
//  DetailViewController.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/27/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import UIKit

// MARK: Properties and Views
/// Controller that manages a screen with a character's details
class DetailViewController: UIViewController {
    // Content Views
    @IBOutlet weak var windowView: UIView?
    @IBOutlet weak var nameView: UIView?
    @IBOutlet weak var scrollView: UIScrollView?

    // Basic Info Views
    @IBOutlet var nameLabels: [UILabel]?
    @IBOutlet var backgroundImageViews: [UIImageView]?
    @IBOutlet weak var descriptionLabel: UILabel?

    // Collection Views
    @IBOutlet weak var comicsCollectionView: UICollectionView?
    @IBOutlet weak var seriesCollectionView: UICollectionView?
    @IBOutlet weak var storiesCollectionView: UICollectionView?
    @IBOutlet weak var eventsCollectionView: UICollectionView?

    // Links Table View
    @IBOutlet weak var linksTableView: UITableView?

    // Layout Information
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint?

    // Managers
    var comicsManager: NamedImageCollectionManager?
    var seriesManager: NamedImageCollectionManager?
    var storiesManager: NamedImageCollectionManager?
    var eventsManager: NamedImageCollectionManager?

    // Model
    var pointer: CharacterPointer?
    var urls: [URLType: NSURL] = [:]
}

// MARK: View Controller Life Cycle Methods
extension DetailViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let details = pointer?.reference else {
            return
        }

        for label in self.nameLabels ?? [] {
            label.text = details.name
        }

        if details.description != "" {
            self.descriptionLabel?.text = details.description
        } else {
            self.descriptionLabel?.superview?.hidden = true
        }

        for imageView in self.backgroundImageViews ?? [] {
            imageView.setMarvelImage(details.image)
        }

        if details.urls.count > 0 {
            self.urls = details.urls
        } else {
            self.linksTableView?.superview?.hidden = true
        }

        // Sets up each collection view individually
        self.comicsManager = NamedImageCollectionManager(items: details.comics)
        self.comicsManager?.configure(self.comicsCollectionView)

        self.seriesManager = NamedImageCollectionManager(items: details.series)
        self.seriesManager?.configure(self.seriesCollectionView)

        self.storiesManager = NamedImageCollectionManager(items: details.stories)
        self.storiesManager?.configure(self.storiesCollectionView)

        self.eventsManager = NamedImageCollectionManager(items: details.events)
        self.eventsManager?.configure(self.eventsCollectionView)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let manager = sender as? NamedImageCollectionManager,
            destination = segue.destinationViewController as? CollectionViewController
            where segue.identifier == "comic_details" {

            destination.collectionManager = manager
            destination.imageIndex = manager.selectedIndex
        }
    }
}

// MARK: Update Methods
extension DetailViewController {
    func animateNavBar(toHeight: CGFloat) {
        self.navBarHeightConstraint?.constant = toHeight
        UIView.animateWithDuration(0.3) { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }
}

// MARK: Action Methods
extension DetailViewController {
    @IBAction func popToHome() {
        self.navigationController?.popViewControllerAnimated(true)
    }
}

// MARK: Scroll View Delegate Methods
extension DetailViewController: UIScrollViewDelegate {
    /**
     Animates the controller's navBar to show or hide according to the current `scrollView` offset.

     Hides when the background image would be seen between the navBar and the contentViews,
     and shows otherwise.

     - parameter scrollView: The controller's main `UIScrollView`
     */
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView != self.scrollView {
            return
        }

        let initial = (self.windowView?.frame.height ?? 0) - 64.0

        let yOffset = scrollView.contentOffset.y

        if yOffset < initial {
            self.animateNavBar(0.0)
        } else if yOffset > initial {
            self.animateNavBar(64.0)
        }
    }
}

// MARK: Collection View Delegate Methods
extension DetailViewController: UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView,
                        didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: false)

        if let manager = collectionView.dataSource as? NamedImageCollectionManager {
            manager.selectedIndex = indexPath.item
            self.performSegueWithIdentifier("comic_details", sender: manager)
        }
    }
}

// MARK: Table View Data Source Methods.
extension DetailViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.urls.count
    }

    func tableView(tableView: UITableView,
                   cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("basic_cell") ?? UITableViewCell()

        cell.textLabel?.text = self.urls.map({ $0.0 })[indexPath.row].rawValue.capitalizedString

        return cell
    }
}

// MARK: Table View Delegate Methods
extension DetailViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        let cell = tableView.cellForRowAtIndexPath(indexPath)

        if let text =  cell?.textLabel?.text?.lowercaseString,
            type = URLType(rawValue: text) {
                UIApplication.sharedApplication().openURL(self.urls[type]!)
        }
    }
}
