//
//  LargeFlowLayout.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/27/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import UIKit

/// Flow Layout Class that paginates scrolling to one item per page
class PaginatedFlowLayout: UICollectionViewFlowLayout {
    var pageSize: CGFloat {
        get {
            return self.itemSize.width + self.minimumLineSpacing
        }
    }

    override func targetContentOffsetForProposedContentOffset(
        proposedContentOffset: CGPoint,
        withScrollingVelocity velocity: CGPoint) -> CGPoint {

        let targetPage = round(proposedContentOffset.x / self.pageSize)
        let targetXOffset = targetPage * self.pageSize

        return CGPoint(x: targetXOffset, y: proposedContentOffset.y)
    }
}
