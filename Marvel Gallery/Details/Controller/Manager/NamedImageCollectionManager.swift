//
//  NamedImageCollectionManager.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/27/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import UIKit

// MARK: Properties and Initialization
/// Manages Collection Views that display images and labels
class NamedImageCollectionManager: NSObject {
    let items: ItemList

    var selectedIndex = 0

    init(items: ItemList) {
        self.items = items
    }
}

// MARK: Basic use methods
extension NamedImageCollectionManager {
    /**
     Registers Cells to be dequeued and sets itself as the `collectionView`'s dataSource

     - parameter collectionView: The `UICollectionView` to be configured.
     - parameter cellNibName:    The name of the `UINib` that describes the cells to be used
     */
    func configure(collectionView: UICollectionView?,
                   cellNibName: String = "NamedImageCell") {
        let nib = UINib(nibName: cellNibName, bundle: nil)
        let loadingNib = UINib(nibName: "LoadingCollectionCell", bundle: nil)

        collectionView?.register(NamedImageCell.self, nib: nib)
        collectionView?.registerNib(loadingNib, forCellWithReuseIdentifier: "loading_cell")

        collectionView?.dataSource = self

        self.items.updateSignal.observeNext({
            collectionView?.reloadData()
        })
    }

    private subscript(index: NSIndexPath) -> CollectionItem {
        get {
            return self.items.loadedList[index.row]
        }
    }
}

// MARK: Collection View Data Source Methods
extension NamedImageCollectionManager: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        var count = self.items.loadedList.count

        if self.items.canLoadMore {
            count += 1
        }

        return count
    }

    func collectionView(collectionView: UICollectionView,
                        cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(indexPath) as NamedImageCell

        let isLastRow = indexPath.row >= self.items.loadedList.count
        if isLastRow {
            self.items.loadMore()
            return collectionView.dequeueReusableCellWithReuseIdentifier("loading_cell",
                                                                         forIndexPath: indexPath)
        }

        if cell.frame.width < 130 {
            cell.imageView?.setMarvelThumbnail(self[indexPath].image)
        } else {
            cell.imageView?.setMarvelImage(self[indexPath].image)
        }

        cell.nameLabel?.text = self[indexPath].name

        return cell
    }
}
