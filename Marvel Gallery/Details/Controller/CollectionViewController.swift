//
//  CollectionViewController.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/27/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import UIKit

// MARK: Properties and Views
/// Controller that manages a screen with a large paginated image gallery with basic descriptions
class CollectionViewController: UIViewController {
    // Collection View
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var collectionLayout: PaginatedFlowLayout?

    // Labels
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var pageCounterLabel: UILabel?

    // Display Information
    var collectionManager: NamedImageCollectionManager?
    var imageIndex: Int = 0

    var images: [CollectionItem]? {
        get {
            return self.collectionManager?.items.loadedList
        }
    }

    var needsLayout = true
}

// MARK: View Controller Life Cycle Methods
extension CollectionViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionManager?.configure(self.collectionView, cellNibName: "LargeImageCell")
        self.collectionView?.delegate = self

        self.collectionManager?.items.updateSignal.observeNext({ [weak self] _ in
            self?.updateLabels()
        })

        self.updateLabels()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if self.needsLayout {
            self.initializeCollectionLayout()
        }
    }
}

// MARK: Sizing and Update Methods
extension CollectionViewController {
    func updateLabels() {
        let totalCount = self.collectionManager?.items.totalSize ?? self.images?.count ?? 0

        if self.imageIndex >= self.images?.count {
            self.nameLabel?.text = ""
            self.pageCounterLabel?.text = ""
        } else {
            self.nameLabel?.text = self.images?[self.imageIndex].name
            self.pageCounterLabel?.text = "\(self.imageIndex + 1)/\(totalCount)"
        }
    }

    /**
     Calculates Item Sizes and Displays the selected Image
     */
    func initializeCollectionLayout() {
        // Make Sure Everything is where it should be and unwrap things
        guard let insets = self.collectionLayout?.sectionInset,
            viewSize = self.collectionView?.frame.size else {
                return
        }

        let aspectRatio: CGFloat = 0.7

        // Ideally we would size our collection items by width,
        // but first we need to make sure everything will fit.
        let idealWidth = viewSize.width - 2 * insets.left

        if idealWidth / aspectRatio <= viewSize.height {
            // Everything fits, so go on sizing by width
            self.collectionLayout?.itemSize = CGSize(width: idealWidth,
                                                     height: idealWidth / aspectRatio)
        } else {
            // Oops, things won't fit so we need to size items by height...
            let height = viewSize.height - insets.top - insets.bottom

            self.collectionLayout?.itemSize = CGSize(width: height * aspectRatio,
                                                     height: height)

            // and make sure our items are centered and properly spaced
            let initialInset = (viewSize.width - height * aspectRatio) / 2

            self.collectionLayout?.sectionInset.left = initialInset
            self.collectionLayout?.sectionInset.right = initialInset
            self.collectionLayout?.minimumLineSpacing = initialInset / 2
        }

        // Either way, show the selected image
        let selectedIndex = NSIndexPath(forItem: self.imageIndex, inSection: 0)
        self.collectionView?.scrollToItemAtIndexPath(selectedIndex,
                                                     atScrollPosition: .CenteredHorizontally,
                                                     animated: false)

        // Make sure we only go through all of this when needed
        self.needsLayout = false
    }
}

// MARK: Action Methods
extension CollectionViewController {
    @IBAction func dismissController() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

// MARK: Delegate Methods
extension CollectionViewController: UICollectionViewDelegate {
    /**
     Hides and Shows the controller's labels according to the collectionView Position.

     The result are animated labels that fade in and out as the user scrolls through images.

     - parameter scrollView: The controllers collectionView
     */
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let xOffset = scrollView.contentOffset.x
        let pageSize = self.collectionLayout?.pageSize ?? 0.0

        let pageOffset = Double(xOffset / pageSize)

        let alpha = CGFloat(1.0 - quadraticTransform(pageOffset % 1.0, delta: 0.15))

        self.nameLabel?.alpha = alpha
        self.pageCounterLabel?.alpha = alpha

        self.imageIndex = Int(round(pageOffset))
        self.updateLabels()
    }
}
