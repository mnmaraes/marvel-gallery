//
//  GradientView.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/27/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import UIKit

/// A simple view that displays a black to clear gradient
class GradientView: UIView {
    lazy var gradientLayer: CAGradientLayer = {
        let layer = CAGradientLayer()

        layer.frame = self.bounds
        layer.colors = [UIColor.blackColor().CGColor, UIColor.clearColor().CGColor]
        layer.opacity = 0.5

        layer.startPoint = CGPoint(x: 0.5, y: 1)
        layer.endPoint = CGPoint(x: 0.5, y: 0)

        return layer
    }()

    override func awakeFromNib() {
        super.awakeFromNib()

        self.layer.insertSublayer(self.gradientLayer, atIndex: 0)
    }
}
