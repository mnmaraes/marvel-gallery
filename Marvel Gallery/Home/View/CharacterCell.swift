//
//  CharacterCell.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/27/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import UIKit

/// A simple cell that displays images and labels with parallax capabilities.
class CharacterCell: UITableViewCell {
    // Subviews
    @IBOutlet weak var backgroundImageView: UIImageView?
    @IBOutlet weak var nameLabel: UILabel?

    // Layout Information
    @IBOutlet weak var topConstraint: NSLayoutConstraint?
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint?

    private let variation: CGFloat = -80

    /**
     Adjusts the cell's parallax according to its vertical position on the screen.

     - parameter position: The normalized (between 0 and 1) position of the cell on the screen.
     */
    func updatePosition(position: CGFloat) {
        let transform = min(max(position, 0), 1)

        self.topConstraint?.constant = self.variation * transform
        self.bottomConstraint?.constant = self.variation * (1 - transform)

        dispatch_async(dispatch_get_main_queue()) {
            self.layoutIfNeeded()
        }
    }
}
