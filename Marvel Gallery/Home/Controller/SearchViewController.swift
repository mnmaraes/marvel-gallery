//
//  SearchViewController.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/27/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import UIKit

import ReactiveCocoa

// MARK: Properties
/// Controller that manages a screen with a searchable lazily loaded table view of characters.
class SearchViewController: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar?
    @IBOutlet weak var resultsTableView: UITableView?

    // Layout Information
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint?

    // Table Manager
    let manager = CharacterTableViewManager(initial: ModelStore.defaultCharacterList())
}

// MARK: View Controller Life Cycle Methods
extension SearchViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        self.manager.updateProducer
            .startWithNext({ [weak self] _ in self?.resultsTableView?.reloadData() })

        self.manager.selectedSignal.observeNext({ [weak self] next in
            self?.performSegueWithIdentifier("search_character_details", sender: next)
        })

        self.resultsTableView?.dataSource = self.manager
        self.resultsTableView?.delegate = self.manager
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        NSNotificationCenter.defaultCenter()
            .addObserver(self,
                         selector: #selector(SearchViewController.keyboardWillAppear),
                         name: UIKeyboardWillShowNotification,
                         object: nil)
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        self.searchBar?.becomeFirstResponder()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let pointer = sender as? CharacterPointer,
            destination = segue.destinationViewController as? DetailViewController
            where segue.identifier == "search_character_details" {

            destination.pointer = pointer
        }
    }
}

// MARK: Update Methods
extension SearchViewController {
    /**
     Updates the controller's tableView frame so it won't be obscured by the keyboard.

     - parameter notification: The keyboard notification
     */
    func keyboardWillAppear(notification: NSNotification) {
        let info = notification.userInfo

        if
            let frameValue = info?[UIKeyboardFrameEndUserInfoKey] as? NSValue,
            curveValue = info?[UIKeyboardAnimationCurveUserInfoKey]?.integerValue,
            curve = UIViewAnimationCurve(rawValue: curveValue),
            duration = info?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {

            self.bottomConstraint?.constant = frameValue.CGRectValue().height
            UIView.animate(curve, duration: duration.doubleValue) {
                self.view.layoutIfNeeded()
            }
        }
    }
}

// MARK: Search Bar Delegate Methods
extension SearchViewController: UISearchBarDelegate {
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        self.manager.requestFilterUpdate(searchText)
    }

    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
