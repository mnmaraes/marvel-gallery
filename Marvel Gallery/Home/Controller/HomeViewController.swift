//
//  HomeViewController.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/22/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import UIKit

// MARK: Properties and View Controller Life Cycle Methods
/// Controller that manages a screen with a lazily loaded table view of character names and images.
class HomeViewController: UIViewController {
    @IBOutlet weak var mainTableView: UITableView?

    let manager = CharacterTableViewManager(initial: ModelStore.defaultCharacterList())

    override func viewDidLoad() {
        super.viewDidLoad()

        self.mainTableView?.contentInset.top = 44.0
        self.mainTableView?.estimatedRowHeight = 136.0
        self.mainTableView?.rowHeight = UITableViewAutomaticDimension

        self.manager.updateProducer.startWithNext({ [weak self] in
            self?.mainTableView?.reloadData()
        })

        self.manager.selectedSignal.observeNext({ [weak self] next in
            self?.performSegueWithIdentifier("character_details", sender: next)
        })

        self.manager.scrollCallBack = { [weak self] in self?.updateCellPositions() }

        self.mainTableView?.dataSource = self.manager
        self.mainTableView?.delegate = self.manager
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        self.updateCellPositions()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let pointer = sender as? CharacterPointer,
            destination = segue.destinationViewController as? DetailViewController
            where segue.identifier == "character_details" {

                destination.pointer = pointer
        }
    }
}

// MARK: Update Methods
extension HomeViewController {
    /**
     Updates each visible cell according to its position so that they can adjust their parallax.
     */
    func updateCellPositions() {
        guard
            let tableView = self.mainTableView,
            visiblePaths = tableView.indexPathsForVisibleRows else {
                return
        }

        let offset = tableView.contentOffset.y
        let height = tableView.bounds.size.height

        for path in visiblePaths {
            let cellRect = tableView.rectForRowAtIndexPath(path)

            let y = cellRect.origin.y
            let cellHeight = cellRect.size.height

            let position = (y + cellHeight - offset) / (height + cellHeight)

            if let cell = tableView.cellForRowAtIndexPath(path) as? CharacterCell {
                cell.updatePosition(position)
            }
        }
    }
}
