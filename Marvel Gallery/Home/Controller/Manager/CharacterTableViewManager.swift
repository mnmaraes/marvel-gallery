//
//  CharacterTableViewManager.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/28/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import Foundation

import ReactiveCocoa
import Result

class CharacterTableViewManager: NSObject {
    private let filterValue = MutableProperty<String>("")
    private let (_characterSignal, _characterPipe) = Signal<CharacterPointer, NoError>.pipe()
    private let (_needsMoreSignal, _needsMorePipe) = Signal<(), NoError>.pipe()

    let filteredList: MutableProperty<CharacterList>

    let updateProducer: SignalProducer<(), NoError>

    var scrollCallBack: (() -> ())? = nil

    init(initial: CharacterList) {
        self.filteredList = MutableProperty(initial)

        self.filteredList <~ self.filterValue.producer
            .map({ ModelStore.characterListForFilter($0) })

        self.updateProducer = self.filteredList.producer
            .observeOn(UIScheduler())
            .flatMap(.Latest, transform: { list in
                return SignalProducer { observer, _ in
                    observer.sendNext()
                    list.updateSignal.observe(observer)
                }
            })

        super.init()

        self._needsMoreSignal
            .throttle(5, onScheduler: QueueScheduler())
            .observeNext({ [weak self] in
                self?.filteredList.value.loadMore()
            })
    }
}

extension CharacterTableViewManager {
    var selectedSignal: Signal<CharacterPointer, NoError> {
        return _characterSignal.observeOn(UIScheduler())
    }

    var characters: CharacterList {
        return self.filteredList.value
    }

    func requestFilterUpdate(newFilter: String) {
        self.filterValue.swap(newFilter)
    }
}

// MARK: Table View Data Source Methods.
extension CharacterTableViewManager: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = self.characters.loadedList.count

        if self.characters.canLoadMore || self.characters.error != nil {
            count += 1
        }

        return count
    }

    func tableView(tableView: UITableView,
                   cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let isLastRow = indexPath.row >= self.characters.loadedList.count
        if self.characters.error != nil && isLastRow {
            return NSBundle.mainBundle()
                .loadNibNamed("ErrorCell",
                              owner: nil,
                              options: nil)[0] as? UITableViewCell ?? UITableViewCell()
        } else if isLastRow {
            self._needsMorePipe.sendNext()
            return NSBundle.mainBundle()
                .loadNibNamed("LoadingCell",
                              owner: nil,
                              options: nil)[0] as? UITableViewCell ?? UITableViewCell()
        }

        let cell = tableView
            .dequeueReusableCellWithIdentifier("character_cell") as? CharacterCell
            ?? CharacterCell()

        let character = self.characters[indexPath.row]

        cell.nameLabel?.text = character.name
        cell.backgroundImageView?.setMarvelImage(character.image)

        return cell
    }
}

// MARK: Table View Delegate Methods
extension CharacterTableViewManager: UITableViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if let callback = self.scrollCallBack {
            callback()
        }
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        let isLastRow = indexPath.row >= self.characters.loadedList.count
        if self.characters.error != nil && isLastRow {
            return self._needsMorePipe.sendNext()
        } else if isLastRow {
            return
        }

        let character = self.characters.loadedList[indexPath.row]
        _characterPipe.sendNext(character)
    }
}
