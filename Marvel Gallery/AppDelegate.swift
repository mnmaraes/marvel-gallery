//
//  AppDelegate.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/21/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

        // Check If Building for testing
        if NSProcessInfo.processInfo().arguments.contains("TestRun") {
            // Set App For Testing
        }
        return true
    }
}
