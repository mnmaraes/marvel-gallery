//
//  Structs.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/28/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import Foundation

enum CollectionType: String {
    case Comic = "comics"
    case Series = "series"
    case Story = "stories"
    case Event = "events"
}

enum URLType: String {
    case Details = "detail"
    case Wiki = "wiki"
    case ComicLink = "comiclink"
}

struct CollectionItem {
    let itemId: Int

    let type: CollectionType

    let name: String
    let image: Image
}

struct Image {
    let path: String
    let ext: String
}

struct MarvelCharacter {
    let characterId: Int

    let name: String
    let description: String

    let image: Image

    let comics: ItemList
    let series: ItemList
    let events: ItemList
    let stories: ItemList

    let urls: [URLType: NSURL]
}
