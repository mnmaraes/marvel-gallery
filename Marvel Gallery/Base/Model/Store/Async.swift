//
//  Async.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/28/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import Foundation

import ReactiveCocoa
import Result

struct ListLoadRequest {
    let offset: Int
    let limit: Int
}

struct ListLoadResult<Data> {
    let total: Int?
    let loadedData: [Data]
}

class AsyncList<Data> {
    typealias LoadAction = Action<ListLoadRequest, ListLoadResult<Data>, ApiError>

    private let _loadAction: LoadAction
    private let _loadSize: Int

    private let workQueue = QueueScheduler(qos: QOS_CLASS_USER_INITIATED, name: "Work Queue")

    private let _loadedList: AnyProperty<[Data]>
    private let _canLoadMore: AnyProperty<Bool>
    private let _totalSize: AnyProperty<Int?>
    private let _error: AnyProperty<ApiError?>

    let updateSignal: Signal<(), NoError>

    init(action: LoadAction, totalSize: Int? = nil, loadSize: Int = 10) {
        self._loadAction = action
        self._loadSize = loadSize

        let valuesSignal = self._loadAction.values

        let stateSignal = valuesSignal.scan([]) { loaded, next in return loaded + next.loadedData }
        let canLoadMoreSignal = valuesSignal.map({ $0.loadedData.count == loadSize }).skipRepeats()

        let totalSizeProducer = SignalProducer<Int?, NoError>({ observer, disposable in
            observer.sendNext(totalSize)
            valuesSignal.map({ $0.total }).observe(observer)
        }).filter({ $0 != nil }).take(1)

        self._loadedList = AnyProperty(initialValue: [], signal: stateSignal)
        self._canLoadMore = AnyProperty(initialValue: true, signal: canLoadMoreSignal)
        self._totalSize = AnyProperty(initialValue: nil, producer: totalSizeProducer)

        let errorsSignal = Signal<ApiError?, NoError>.merge([
            valuesSignal.map({ _ in return nil }),
            self._loadAction.errors.map({ $0 as ApiError? })
        ])

        self._error = AnyProperty(initialValue: nil, signal: errorsSignal)

        self.updateSignal = errorsSignal.map({ _ in return ()}).observeOn(UIScheduler())
    }
}

// MARK: Basic Usage Methods
extension AsyncList {
    var loadedList: [Data] {
        return self._loadedList.value
    }

    var canLoadMore: Bool {
        return self._canLoadMore.value
    }

    var totalSize: Int? {
        return self._totalSize.value
    }

    var error: ApiError? {
        return self._error.value
    }

    func loadMore() {
        let request = ListLoadRequest(offset: self.loadedList.count, limit: self._loadSize)
        self._loadAction.apply(request).startOn(workQueue).start()
    }
}

extension AsyncList where Data: Pointer {
    subscript(index: Int) -> Data.Object {
        return self.loadedList[index].reference
    }
}
