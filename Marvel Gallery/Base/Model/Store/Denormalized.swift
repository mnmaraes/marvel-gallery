//
//  Denormalized.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/28/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import Foundation

protocol Addressable {
    associatedtype Address: Hashable

    func getAddress() -> Address
}

protocol Pointer {
    associatedtype Object: Addressable

    var reference: Object { get }
}

extension MarvelCharacter: Addressable {
    func getAddress() -> Int {
        return self.characterId
    }
}

extension CollectionItem: Addressable {
    func getAddress() -> String {
        return self.type.rawValue + ":\(self.itemId)"
    }
}
