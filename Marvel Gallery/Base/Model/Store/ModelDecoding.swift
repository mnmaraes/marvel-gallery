//
//  ModelDecoding.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/28/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import Foundation

import JASON

extension JSONKeys {
    static let idKey = JSONKey<Int>("id")

    // Image Decoding
    static let imagePath = JSONKey<String>("path")
    static let imageExtension = JSONKey<String>("extension")

    // Character Decoding
    static let name = JSONKey<String>("name")
    static let description = JSONKey<String>("description")

    static let imageJSON = JSONKey<JSON>("thumbnail")

    static let comicsCount = JSONKey<Int?>(path: "comics", "available")
    static let seriesCount = JSONKey<Int?>(path: "series", "available")
    static let eventsCount = JSONKey<Int?>(path: "events", "available")
    static let storiesCount = JSONKey<Int?>(path: "stories", "available")

    static let urls = JSONKey<[JSON]>("urls")

    // Item Decoding
    static let title = JSONKey<String>("title")
}

extension NSData {
    func toJson() -> JSON? {
        guard let object = try? NSJSONSerialization.JSONObjectWithData(self, options: []) else {
            return nil
        }

        return JSON(object)
    }
}

extension Image {
    static func decode(json: JSON) -> Image {
        return Image(path: json[.imagePath],
                     ext: json[.imageExtension])
    }
}

extension CollectionItem {
    static func decode(json: JSON, type: CollectionType) -> CollectionItem {
        return CollectionItem(itemId: json[.idKey],
                              type: type,
                              name: json[.title],
                              image: Image.decode(json[.imageJSON]))
    }
}

extension MarvelCharacter {
    static func decode(json: JSON) -> MarvelCharacter {
        let urls: [URLType: NSURL] = json[.urls].reduce([:]) { acc, urlJson in
            var dictionary = acc

            if let type = URLType(rawValue: urlJson["type"].stringValue),
                url = urlJson["url"].nsURL {
                dictionary[type] = url
            }

            return dictionary
        }

        let id = json[.idKey]

        return MarvelCharacter(characterId: id,
                               name: json[.name],
                               description: json[.description],
                               image: Image.decode(json[.imageJSON]),
                               comics: ModelStore.newList(id,
                                type: .Comic,
                                capacity: json[.comicsCount]),
                               series: ModelStore.newList(id,
                                type: .Series,
                                capacity: json[.seriesCount]),
                               events: ModelStore.newList(id,
                                type: .Event,
                                capacity: json[.eventsCount]),
                               stories: ModelStore.newList(id,
                                type: .Story,
                                capacity: json[.storiesCount]),
                               urls: urls)
    }
}
