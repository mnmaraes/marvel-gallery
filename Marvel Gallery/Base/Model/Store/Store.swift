//
//  Async.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/28/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import Foundation

import ReactiveCocoa
import Result

typealias CharacterList = AsyncList<CharacterPointer>
typealias ItemList = AsyncList<CollectionItem>

struct ModelStore {
    private typealias CharacterListCache = MutableProperty<[String: CharacterList]>

    private typealias CharacterCache = MutableProperty<[Int: MarvelCharacter]>

    private static let listCache = CharacterListCache([:])
    private static let characterCache = CharacterCache([:])
}

// MARK: Basic Usage Methods
extension ModelStore {
    static func defaultCharacterList() -> CharacterList {
        return self.characterListForFilter("")
    }

    static func characterListForFilter(filter: String) -> CharacterList {
        listCache.modify({ value in
            if let _ = value[filter] {
                return value
            }

            var newValue = value

            newValue[filter] = self.newList(filter)
            return newValue
        })

        return listCache.value[filter]!
    }
}

// MARK: Helper Methods
extension ModelStore {
    static func newList(characterId: Int,
                        type: CollectionType,
                        capacity: Int?) -> ItemList {

        return ItemList(action: Action { request in

            return ApiCaller.sharedCaller.characterCollection(characterId,
                collectionName: type.rawValue,
                limit: request.limit,
                offset: request.offset)
                .retry(3)
                .attemptMap({ data in
                    guard let json = data.toJson()?["data"].json,
                        results = json["results"].jsonArray else {
                            return Result(error: .DecodingError)
                    }

                    let total = json["total"].int
                    let items = results.map({ CollectionItem.decode($0, type: type) })

                    return Result(ListLoadResult(total: capacity ?? total, loadedData: items))
                })

            }, totalSize: capacity)
    }

    static func newList(filter: String) -> CharacterList {
        return CharacterList(action: Action { request in

            return ApiCaller.sharedCaller.characterList(request.limit,
                offset: request.offset,
                filter: filter)
                .retry(3)
                .attemptMap({ data in
                    guard let json = data.toJson()?["data"].json,
                        results = json["results"].jsonArray else {
                        return Result(error: .DecodingError)
                    }

                    let total = json["total"].int
                    let characters = results.map({ MarvelCharacter.decode($0) })

                    return Result((total, characters))
                })
                .map({ (total: Int?,
                    characters: [MarvelCharacter]) -> ListLoadResult<CharacterPointer> in
                        let pointers = ModelStore.store(characters)
                        return ListLoadResult(total: total, loadedData: pointers)
                })
        })
    }
}

class CharacterPointer: Pointer {
    let address: Int

    var reference: MarvelCharacter {
        return ModelStore.retrieve(self.address)
    }

    private init(address: Int) {
        self.address = address
    }
}

extension ModelStore {
    static func store(characters: [MarvelCharacter]) -> [CharacterPointer] {
        self.characterCache.modify({ value in
            var newValue = value

            for character in characters where value[character.getAddress()] == nil {
                newValue[character.getAddress()] = character
            }

            return newValue
        })

        return characters.map({ CharacterPointer(address: $0.getAddress()) })
    }

    static func retrieve(address: Int) -> MarvelCharacter {
        return self.characterCache.value[address]!
    }
}
