//
//  ApiManager.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/28/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import Foundation

import ReactiveCocoa

/**
 Enum for possible Api Errors

 - BadResponseError: The Api Has returned an unexpected status code
 - DecodingError: Couldn't Decode the Response.
 - NoDataError: The response has returned no data.
 - ResponseError: The response of the call was an error. Carries the returned NSError.
 */
enum ApiError: ErrorType {
    case BadResponseError(Int)
    case DecodingError
    case NoDataError
    case ResponseError(NSError)

    var description: String {
        switch self {
        case .ResponseError(let error):
            return error.localizedDescription
        case .BadResponseError(let code):
            return "Bad Response: \(code)"
        case .DecodingError:
            return "Error while trying to decode returned data"
        case .NoDataError:
            return "No Data Returned from call"
        }
    }
}

/**
 *  Static Keys for reading the ApiKeys.plist config file
 */
struct ConfigKeys {
    static let privateKey = "PrivateKey"
    static let publicKey = "PublicKey"
}


/**
 *  Static URLs that map to the Marvel Endpoint
 */
struct MarvelAPI {
    static let baseUrl = NSURL(string: "http://gateway.marvel.com:80/")!

    static func characterListUrl() -> NSURL {
        return baseUrl
            .URLByAppendingPathComponent("/v1/public/characters")
    }

    static func characterUrl(characterId: Int) -> NSURL {
        return characterListUrl()
            .URLByAppendingPathComponent("\(characterId)")
    }

    static func characterCollectionUrl(characterId: Int,
                                       collectionName: String) -> NSURL {
        return characterUrl(characterId)
            .URLByAppendingPathComponent(collectionName)
    }
}

/**
 *  Object Responsible for making Calls to a Remote Api
 */
// MARK: Properties and Initialization
struct ApiCaller {
    /// The Default Caller object
    static let sharedCaller = ApiCaller()

    private let publicKey: String
    private let privateKey: String

    private init() {
        guard
            let configPath = NSBundle.mainBundle().pathForResource("ApiKeys", ofType: "plist"),
            config = NSDictionary(contentsOfFile: configPath),
            publicKey = config[ConfigKeys.publicKey] as? String,
            privateKey = config[ConfigKeys.privateKey] as? String else {

                // Initialization should fail if Api Keys not properly configured
                assert(false, "ApiKeys not properly configured!")
                self.publicKey = ""
                self.privateKey = ""
        }

        self.publicKey = publicKey
        self.privateKey = privateKey
    }
}

// MARK: Basic Usage Methods
extension ApiCaller {
    func characterList(limit: Int,
                       offset: Int? = nil,
                       filter: String? = nil) -> SignalProducer<NSData, ApiError> {

        var params: [String: AnyObject] = ["limit": limit]

        if let offset = offset { params["offset"] = offset }
        if let filter = filter where filter != "" { params["nameStartsWith"] = filter }

        return self.call(MarvelAPI.characterListUrl(), params: params)
    }

    func characterCollection(characterId: Int,
                             collectionName: String,
                             limit: Int,
                             offset: Int? = nil) -> SignalProducer<NSData, ApiError> {
        var params: [String: AnyObject] = ["limit": limit]

        if let offset = offset { params["offset"] = offset }

        let url = MarvelAPI.characterCollectionUrl(characterId, collectionName: collectionName)

        return self.call(url, params: params)
    }
}

// MARK: Helper Methods
extension ApiCaller {
    private var basicParams: [String: AnyObject] {
        let timestamp = "\(NSDate().timeIntervalSince1970)"
        let md5 = (timestamp + self.privateKey + self.publicKey).md5()!
        return [
            "ts": timestamp,
            "apikey": self.publicKey,
            "hash": md5
        ]
    }

    private func call(url: NSURL, params: [String: AnyObject]) -> SignalProducer<NSData, ApiError> {
        return SignalProducer { observer, disposable in
            let url = url.URLByAppendingParams(self.basicParams).URLByAppendingParams(params)

            NSURLSession.sharedSession()
                .dataTaskWithURL(url, completionHandler: { (data, response, error) in
                    print(data, response, error)

                    if let error = error {
                        observer.sendFailed(.ResponseError(error))
                        return
                    }

                    if let response = response as? NSHTTPURLResponse
                        where response.statusCode != 200 {

                        observer.sendFailed(.BadResponseError(response.statusCode))
                        return
                    }

                    if let data = data {
                        observer.sendNext(data)
                        observer.sendCompleted()

                        return
                    }

                    observer.sendFailed(.NoDataError)
                }).resume()
        }
    }
}
