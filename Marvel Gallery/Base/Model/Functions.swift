//
//  Functions.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/27/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import Foundation

/**
 A quandratic transformation function.

 - parameter input:     The value to be transformed. Normally a value between 0 and 1.
 - parameter delta:     The interval before values should be transformed. Defaults to 0.
 - parameter amplitude: The maximum value of the output. Defaults to 1.

 - returns: The transformed value. A value between 0 and `amplitude`
 */
func quadraticTransform(input: Double, delta: Double = 0.0, amplitude: Double = 1.0) -> Double {
    let start: Double = 0.0 + delta
    let end: Double = 1.0 - delta
    let height: Double = amplitude / ((0.5 - start) * (0.5 - end))

    let y = (input - start) * (input - end) * height

    return max(y, 0.0)
}
