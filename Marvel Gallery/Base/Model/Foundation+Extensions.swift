//
//  Foundation+Extensions.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/28/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import Foundation

extension String {
    func md5() -> String? {
        let byteLength = CC_LONG(self.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))
        let digestLength = Int(CC_MD5_DIGEST_LENGTH)

        guard
            let cString = self.cStringUsingEncoding(NSUTF8StringEncoding),
            resultData = NSMutableData(length: digestLength) else {
            return nil
        }

        CC_MD5(cString, byteLength, UnsafeMutablePointer<CUnsignedChar>(resultData.mutableBytes))

        var hash = ""

        let bytesPointer = UnsafePointer<CUnsignedChar>(resultData.bytes)
        let iterableBuffer = UnsafeBufferPointer<CUnsignedChar>(start: bytesPointer,
                                                                count: resultData.length)

        for byte in iterableBuffer {
            hash = hash.stringByAppendingFormat("%02x", byte)
        }

        return hash
    }
}

extension NSURL {
    func URLByAppendingParams(params: [String: AnyObject]) -> NSURL {
        let components = NSURLComponents(URL: self, resolvingAgainstBaseURL: true)!

        let newItems = params.map({ key, value in
            return NSURLQueryItem(name: key, value: "\(value)")
        })

        if let items = components.queryItems {
            components.queryItems = items + newItems
        } else {
            components.queryItems = newItems
        }

        return components.URL!
    }
}
