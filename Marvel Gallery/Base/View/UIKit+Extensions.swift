//
//  UIKit+Extensions.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/27/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import UIKit

import Kingfisher

extension UIView {
    static func animate(curve: UIViewAnimationCurve,
                        duration: NSTimeInterval,
                        @noescape animations: () -> Void) {

        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(duration)
        UIView.setAnimationCurve(curve)

        animations()

        UIView.commitAnimations()
    }
}

extension UITableView {
    func register<C: UITableViewCell>(cellType: C.Type, nib: UINib? = nil) {
        if let nib = nib {
            self.registerNib(nib, forCellReuseIdentifier: NSStringFromClass(cellType))
        } else {
            self.registerClass(cellType, forCellReuseIdentifier: NSStringFromClass(cellType))
        }
    }

    func dequeue<C: UITableViewCell>(indexPath: NSIndexPath) -> C {
        return self
            .dequeueReusableCellWithIdentifier(
                NSStringFromClass(C.self),
                forIndexPath: indexPath
            ) as? C
            ?? C()
    }
}

extension UICollectionView {
    func register<C: UICollectionViewCell>(cellType: C.Type, nib: UINib? = nil) {
        if let nib = nib {
            self.registerNib(nib, forCellWithReuseIdentifier: NSStringFromClass(cellType))
        } else {
            self.registerClass(cellType, forCellWithReuseIdentifier: NSStringFromClass(cellType))
        }
    }

    func dequeue<C: UICollectionViewCell>(indexPath: NSIndexPath) -> C {
        return self.dequeueReusableCellWithReuseIdentifier(
            NSStringFromClass(C.self),
            forIndexPath: indexPath
            ) as? C
            ?? C()
    }
}

extension UIImageView {
    func setMarvelImage(image: Image) {
        let url = NSURL(string: image.path + "." + image.ext) ?? NSURL()

        self.kf_setImageWithURL(url)
    }

    func setMarvelThumbnail(image: Image) {
        let url = NSURL(string: image.path + "/portrait_xlarge." + image.ext) ?? NSURL()

        self.kf_setImageWithURL(url)
    }
}
