//
//  ModelDecodingTests.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/28/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import XCTest

@testable import Marvel_Gallery

class ModelDecodingTests: XCTestCase {

    override func setUp() {
        super.setUp()

    }

    override func tearDown() {

        super.tearDown()
    }

    func testMarvelCharacterDecoding() {
        let path = NSBundle(forClass: ModelDecodingTests.self)
            .pathForResource("captain_america", ofType: "json")


        let json = NSData(contentsOfFile: path!)!.toJson()!

        let theCap = MarvelCharacter.decode(json)

        XCTAssertEqual(theCap.characterId, 1009220)
        XCTAssertEqual(theCap.name, "Captain America")
        XCTAssertEqual(theCap.urls.count, 3)
        XCTAssertEqual(theCap.image.path,
                       "http://i.annihil.us/u/prod/marvel/i/mg/3/50/537ba56d31087")
    }

    func testMarvelCharacterListDecoding() {
        let path = NSBundle(forClass: ModelDecodingTests.self)
            .pathForResource("captains", ofType: "json")


        let json = NSData(contentsOfFile: path!)!.toJson()!

        let captains = json["data"]["results"].jsonArrayValue.map({ MarvelCharacter.decode($0) })

        XCTAssertEqual(captains.count, 10)

        for captain in captains {
            XCTAssertNotEqual(captain.characterId, 0)
            XCTAssertNotEqual(captain.name, "")
            XCTAssertGreaterThan(captain.urls.count, 0)
            XCTAssertNotEqual(captain.image.path, "")
            XCTAssertNotEqual(captain.image.ext, "")
        }
    }

}
