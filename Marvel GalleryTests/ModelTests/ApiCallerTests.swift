//
//  ApiCallerTests.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/28/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import XCTest

@testable import Marvel_Gallery

class ApiCallerTests: XCTestCase {

    override func setUp() {
        super.setUp()

    }

    override func tearDown() {

        super.tearDown()
    }

    func testCharacterListCall() {
        let expectation = expectationWithDescription("Data Being Returned")

        ApiCaller.sharedCaller.characterList(10).startWithNext({ data in
            expectation.fulfill()
        })

        waitForExpectationsWithTimeout(10, handler: nil)
    }

    func testCharacterCollectionListCall() {
        let expectation = expectationWithDescription("Data Being Returned")

        ApiCaller.sharedCaller.characterCollection(1009220, collectionName: "comics", limit: 10)
            .startWithNext({ data in
                expectation.fulfill()
            })

        waitForExpectationsWithTimeout(10, handler: nil)
    }
}
