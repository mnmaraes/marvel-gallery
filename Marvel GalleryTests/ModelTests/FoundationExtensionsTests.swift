//
//  FoundationExtensionsTests.swift
//  Marvel Gallery
//
//  Created by Murillo Nicacio de Maraes on 4/28/16.
//  Copyright © 2016 SuperUnreasonable. All rights reserved.
//

import XCTest

@testable import Marvel_Gallery

class FoundationExtensionsTests: XCTestCase {

    override func setUp() {
        super.setUp()

    }

    override func tearDown() {

        super.tearDown()
    }

    func testMD5Hashing() {
        let hashDictionary = [
            "Murillo Nicacio de Maraes": "ef0995bb52439a1bc564097c5226eb03",
            "Marvel Gallery": "a6dea22d822470ad97ce32ef3690a9d5",
            "Hole19": "deda5938ddbba084aa440ada3e9d3535"
        ]

        for (string, md5) in hashDictionary {
            XCTAssertEqual(string.md5(), md5)
        }
    }

    func testURLParams() {
        let expectedUrlString = NSURL(string: "http://www.example.com/path?name=Murillo&game=iOS")
        let baseUrl = NSURL(string: "http://www.example.com")
        let path = "path"

        let params = ["name": "Murillo", "game": "iOS"]

        let resultingUrl = NSURL(string: path, relativeToURL: baseUrl)!.URLByAppendingParams(params)

        XCTAssertEqual(resultingUrl.baseURL, expectedUrlString?.baseURL)
        XCTAssertEqual(resultingUrl.path, expectedUrlString?.path)
        XCTAssertEqual(resultingUrl.query, resultingUrl.query)
    }

}
