# Marvel Gallery

A simple Marvel Gallery App creted for Hole19 as an interview challenge.

## Build and Run

To Run this code you need a set of API keys for the Marvel API. 
You can get them for free [here](http://developer.marvel.com/).

To configure the project with your keys:

1. `cd [project dir]/Marvel\ Gallery`

2. `cp ApiKeys_Template.plist ApiKeys.plist`

3. Open `ApiKeys.plist` with XCode and fill in your keys.

You will also need to install a couple of dependencies.

To do so:

1. [Install Carthage](https://github.com/Carthage/Carthage#installing-carthage)

2. `cd [project dir]`

3. run `carthage update`

You can now build and run the project normally.


## Specifications

The specifications and requirements of the project can be found [here](https://h19-file-store.s3.amazonaws.com/Coding%20Challenge/h19-marvel-code-challenge.pdf).

## Design Discussion and Roadmap

### Wed, Apr 27th 2016

- [x] Get UI ready
    - [x] Complete UI Behaviors
        - [x] Home Screen
- [x] Code Cleanup
- [ ] UI Test Suite


### Tue, Apr 26th 2016

UI Behaviors

- [ ] Get UI ready
    - [x] Layout Comic Screen
    - [ ] Complete UI Behaviors
        - [x] Comic Screen
        - [x] Details Screen
        - [ ] Home Screen    

### Mon, Apr 25th 2016

Some more UI Work

- [ ] Get UI ready and Looking Great
    - [x] Layout details screen

### Fri, Apr 22nd 2016

UI Work

- [ ] Get UI ready and Looking Great
    - [x] Build a Functioning Home Screen

### Thu, Apr 21st 2016

Project Setup.

- [x] Project Creation
- [x] Readme Creation
- [x] Repo Creation
    - [x] First Commit
- [x] Import Assets